#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <ctype.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <algorithm>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <vector>
#include <sstream>
#include <fstream>
#define DOS_PUNTOS ':'
#define E_BLANCO ' '

using namespace std;

/*---Structuras utilizadas----*/

struct dataMuestra {
  int bandejaDeEntradaId;
  int muestraId;
  char tipoDeMuestra;
  int cantidadMuestra;
  int posicionEnCola;
  int estado;
};

struct bandejaEntrada {
  int bandejaDeEntradaId;
  int cantidadDeMuestras;
};

struct bandejaSalida {
  int bandejaDeSalidaId;
  int cantidadDeMuestras;
};

struct reactivos {
  int NIVELREACTSANGRE = 100;
  int NIVELREACTDENDRITOS = 100;
  int NIVELREACTPIEL = 100;
};

/*------Variables----------------*/

int NUMENTRADAS = 5;
int NUMPOSCENTRADA = 6;
int NUMPOSCSALIDA = 10;
string NOMBSEGMEMORIA = "evaluator";
int NIVELREACTSANGRE = 100;
int NIVELREACTDENDRITOS = 100;
int NIVELREACTPIEL = 100;
int opt;
string input = "";
vector<string> entradaParse;
vector<dataMuestra> colaEntrada;
vector<dataMuestra> colaSalida;
vector<bandejaEntrada> Entrada;
vector<bandejaSalida> Salida;
reactivos react;

// Divide una cadena en función de un separador enviado como parámetro
vector<string>splitInput(string str, char separator) {
  for (int i = 0; i < str.length(); i++) {
    if (str[i] == separator) {
      str[i] = E_BLANCO;
    }
  }
  vector<string> array;
  stringstream ss(str);
  string temp;
  while (ss >> temp) {
    array.push_back(temp);
  }
  return array;
}

bool esNumero (string & s, bool esNegativo) {

  for (int i = 0; i < s.length(); i++) {

    if (esNegativo) {

      //SI el numero es negativo
      if (s[i] == '-' && i != 0) return false;

      if (! (s[i] >= '0' && s[i] <= '9' || s[i] == ' '
             || s[i] == '-') ) return false;

    } else {
      if (! (s[i] >= '0' && s[i] <= '9' || s[i] == ' ') ) return false;
    }

  }

  return true;

}

bool inputValidacion(vector<string> entradaParse) {

  /* Parametros que el usuario ingreso
    - Bandeja de entrada
    - Letra que indica la muestra
    - cantidad de la muestra
  */
  if (entradaParse.size() != 3) {
    cout << "Error al ingresar parametros" << endl;
    return false;
  }

  string parametro1 = entradaParse.at(0);
  int bandeja;
  stringstream(parametro1) >> bandeja;

  // El valor de Bandeja no puede ser mayor al #bandejas
  if (bandeja > NUMENTRADAS - 1) {
    cout << "Error en el número de bandeja" << endl;
    return false;
  }
  char parametro2 = entradaParse[0][1]; //Debe ser una letra B|D|S
  // El valor de la letra no puede ser distinto de  B|D|S
  if (parametro2 != 'B' | parametro2 != 'D' | parametro2 != 'S' | parametro2 != 'b' | parametro2 != 'd' | parametro2 != 's') {
    return false;
    cout << "se murio en los parametros reg";
  }
  string parametro3 = entradaParse.at(2);//Debe ser un numero entre 1-5
  int cantidadMuestra;
  stringstream(parametro3) >> cantidadMuestra;

  //La muestra debe estar entre 1 y 5
  if (cantidadMuestra > 5)
  {
    cout << "La cantidad de la muestra es mayor a 5" << endl;
    return false;
  }
  return true;
}

bool Wait(const unsigned long &Time) {

  clock_t Tick = clock_t(float(clock()) / float(CLOCKS_PER_SEC) * 1000.f);
  if (Tick < 0) return 0;
  clock_t Now = clock_t(float(clock()) / float(CLOCKS_PER_SEC) * 1000.f);
  if (Now < 0) return 0;

  while ((Now - Tick) < Time) {
    Now = clock_t(float(clock()) / float(CLOCKS_PER_SEC) * 1000.f);
    if (Now < 0) return 0;
  }
  return 1;
}


void listProc() {
  cout << "camposentrada " << NUMPOSCENTRADA << endl;
  for (int i = 0; i < NUMPOSCENTRADA; i++) {
    cout << "i: " << NUMPOSCENTRADA << endl;
    if (colaEntrada.at(i).estado == 1) {
      cout << "\n";
      cout << "identificador: " << colaEntrada.at(i).muestraId << endl;
      cout << "cola de entrada: " << colaEntrada.at(i).bandejaDeEntradaId << endl;
      cout << "tipo de muestra: " << colaEntrada.at(i).tipoDeMuestra << endl;
      cout << "cantidad de muestra: " << colaEntrada.at(i).cantidadMuestra << endl;
      cout << "procesamiento en s: " << endl;
      cout << "\n";
    }
  }
}

void listWaiting() {
  cout << NUMPOSCENTRADA;
  cout << colaEntrada.size();
  for (int i = 0; i < NUMPOSCENTRADA; i++) {
    if (colaEntrada.at(i).estado == 0) {
      cout << "\n";
      cout << "identificador: " << colaEntrada.at(i).muestraId << endl;
      cout << "cola de entrada: " << colaEntrada.at(i).bandejaDeEntradaId << endl;
      cout << "tipo de muestra: " << colaEntrada.at(i).tipoDeMuestra << endl;
      cout << "cantidad de muestra: " << colaEntrada.at(i).cantidadMuestra << endl;
      cout << "procesamiento en s: " << endl;
      cout << "\n";
    }
  }
}

void listReported() {
  for (int i = 0; i < NUMPOSCENTRADA; i++) {
    if (colaEntrada.at(i).estado == 2) {
      cout << "\n";
      cout << "identificador: " << colaEntrada.at(i).muestraId << endl;
      cout << "cola de entrada: " << colaEntrada.at(i).bandejaDeEntradaId << endl;
      cout << "tipo de muestra: " << colaEntrada.at(i).tipoDeMuestra << endl;
      cout << "cantidad de muestra: " << colaEntrada.at(i).cantidadMuestra << endl;
      cout << "procesamiento en s: " << endl;
      cout << "\n";
    }
  }
}

void listReact() {
  cout << "\n";
  cout << "Nivel de dendritos: " << react.NIVELREACTSANGRE << endl;
  cout << "Nivel de piel: " << react.NIVELREACTDENDRITOS << endl;
  cout << "Nivel de sangre: " << react.NIVELREACTPIEL << endl;
  cout << "\n";
}


void regMuestra(int line_number) {
  dataMuestra muestra;

  muestra.muestraId = line_number;
  muestra.bandejaDeEntradaId = stoi(entradaParse[0]);
  muestra.tipoDeMuestra = entradaParse[1].at(0);
  muestra.cantidadMuestra = stoi(entradaParse[2]);
  muestra.estado = 1;

  for (int i = 0; i < NUMENTRADAS; i++) {
    if (Entrada.at(i).bandejaDeEntradaId == line_number) {
      int posicion = colaEntrada.size();
      muestra.posicionEnCola = posicion;
      colaEntrada.push_back(muestra);
    }
  }
}

int main (int argc, char *argv[])
{
  //Segmem_init
  /*Bandeja de Entrada y Evaluador
  //semaforo de mutex para poder realizar el "empaquetamiento" de la muestra
  //semaforos de fillCount y emptyCount, fillCount para saber cuantos espacios existen en el buffer y empytCount para saber el numero de espacios disponibles
  */
  sem_t mutexEntrada;
  sem_t fullCountEntrada;
  sem_t emptyCountEntrada;

  sem_init(&mutexEntrada, 0, 1);
  sem_init(&fullCountEntrada, 0, 0);
  sem_init(&emptyCountEntrada, 0, 16384); //Tamano buffer 2^14


  //Bandeja de Evaluador y bandeja de salida
  sem_t mutexSalida;
  sem_t fullCountSalida;
  sem_t emptyCountSalida;

  sem_init(&mutexSalida, 0, 1);
  sem_init(&fullCountSalida, 0, 0);
  sem_init(&emptyCountSalida, 0, 16384);

  sem_t mutexReactivos;
  sem_init(&mutexReactivos, 0, 1);

  int shmfd;
  shmfd = shm_open("evaluator", O_RDWR | O_CREAT , 0660);

  if (shmfd < 0) {

    perror("shm_open");
    exit(1);
  }

  size_t len = sizeof(mutexEntrada) + sizeof(fullCountEntrada) + sizeof(emptyCountEntrada) + sizeof(mutexSalida) +
               sizeof(fullCountSalida) + sizeof(emptyCountSalida) + sizeof(bandejaEntrada) * NUMENTRADAS + sizeof(bandejaSalida) * NUMPOSCSALIDA + sizeof(dataMuestra) * 1000 + sizeof(reactivos);

  void *startshm;
  if ((startshm = mmap(NULL, len, PROT_READ | PROT_WRITE,
                       MAP_SHARED, shmfd, 0)) == MAP_FAILED) {
    perror("mmap");
    exit(1);
  }

  for (int i = 0; i < NUMENTRADAS; i++) {
    bandejaEntrada bandeja;
    bandeja.bandejaDeEntradaId = i;
    bandeja.cantidadDeMuestras = NUMPOSCENTRADA;
    Entrada.push_back(bandeja);
  }

  for (int i = 0; i < NUMPOSCSALIDA; i++) {
    bandejaSalida bandeja;
    bandeja.bandejaDeSalidaId = i;
    bandeja.cantidadDeMuestras = NUMPOSCENTRADA;
    Salida.push_back(bandeja);
  }


  /*-------------------------------------------------------------------------------------------------------------------------------------------*/
  if (string(argv[1]) == "init") {
    int index;
    int c;
    opterr = 0;
    while ((c = getopt (argc, argv, "i:n:b:s:d:e:o:")) != -1)
      switch (c) {
      //Numero de entradas
      case 'i':
        NUMENTRADAS = atoi(optarg);
        if (NUMENTRADAS > 1)
        { printf ("opcion -i  valida con valor `%d'\n", NUMENTRADAS);}
        else { printf ("Parametro -i  invalido\n");}
        break;
      //Numero de posiciones en cola de entrada
      case 'e':
        NUMPOSCENTRADA = atoi(optarg);
        { printf ("opcion -e  valida con valor `%d'\n", NUMPOSCENTRADA);}
        break;
      //Numero de posiciones en cola de salida
      case 'o':
        NUMPOSCSALIDA = atoi(optarg);
        { printf ("opcion -o  valida con valor `%d'\n", NUMPOSCSALIDA);}
        break;
      //Nombre del segmento de Memoria
      case 'n':
        NOMBSEGMEMORIA = optarg;
        printf ("opcion -n con valor `%s'\n", optarg);
        break;
      //Nivel  de reactivo de dendritos
      case 'b':
        NIVELREACTDENDRITOS = atoi(optarg);
        if (NIVELREACTDENDRITOS <= 100)
        { printf ("opcion -b  valida con valor `%d'\n", NIVELREACTDENDRITOS);}
        else
        { printf ("Parametro -b  invalido por ser mayor a 100\n");}
        break;

      //Nivel de reactivo en la piel
      case 's':
        NIVELREACTPIEL = atoi(optarg);
        if (NIVELREACTPIEL <= 100) {
          printf ("opcion -s  valida con valor `%d'\n", NIVELREACTPIEL);
        }
        else
        { printf ("Parametro -s  invalido por ser mayor a 100\n"); }
        break;
      //Nivel de reactivo en sangre
      case 'd':
        NIVELREACTSANGRE = atoi(optarg);
        if (NIVELREACTSANGRE <= 100) {
          printf ("opcion -d  valida con valor `%d'\n", NIVELREACTSANGRE);
        }
        else {
          printf ("Parametro -d  invalido por ser mayor a 100\n");
        }
        break;
      case '?':

        if (isprint (optopt))

          fprintf (stderr, "Opcion desconocida `-%c'.\n", optopt);
        else
          fprintf (stderr, "Opcion del caracter desconocido `\\x%x'.\n",
                   optopt);

        sem_wait(&mutexReactivos);
        react.NIVELREACTSANGRE;
        react.NIVELREACTDENDRITOS;
        react.NIVELREACTPIEL;
        sem_post(&mutexReactivos);


        return 1;
      default:
        abort ();
      }

  } else if (string(argv[1]) == "reg") {
    int index;
    int c;
    string input;
    opterr = 0;
    int line_number = 0;
    while ((c = getopt (argc, argv, "n:")) != -1) {
      switch (c) {
      case'n':
        if (optarg == NOMBSEGMEMORIA) {

          bool esCorrecto = true;

          while (getline(cin, input)) {
            //ver la salida de archivo
            line_number ++;
            //cout<<"line "<< line_number
            //  <<",input = \""<<input<<"\"\n";
            /* El condicional termina con 0 */
            if (input == "0") {
              break;
            }
            if (input.empty()) {
              continue;
            }
            if (count(input.begin(), input.end(), ':') != 2) {
              cout << "error" << endl;
              continue;
            }
            entradaParse = splitInput(input, DOS_PUNTOS);
            esCorrecto = true; //

            if (esCorrecto == true) {
              //Aqui se hace la asignacion de memoria en bandeja
              sem_wait(&emptyCountEntrada);
              sem_wait(&mutexEntrada);
              regMuestra(line_number);
              sem_post(&mutexEntrada);
              sem_post(&fullCountEntrada);
            }
          }
        } else { cout << "Existe un error en el nombre del segmento de Memoria" << endl;}
        break;
      }
    }

  } else if (string(argv[1]) == "ctrl") {
    int index;
    int c;
    opterr = 0;
    string controlInput = "";
    while ((c = getopt (argc, argv, "s:")) != -1) {
      switch (c) {

      case's':
        if (optarg == NOMBSEGMEMORIA) {
          cout << "Puede ingresar los subcomandos list o update:" << endl;
          cout << "list[processing|waiting|reported|reactive|all]" << endl;
          cout << "\n";

          cout << "salir del programa con '0'" << endl;
          cout << "\n";

          while (getline(cin, controlInput)) {
            /* El condicional termina con 0 */
            if (controlInput == "0") {
              break;
            }
            if (controlInput.empty()) {
              continue;
            }

            if (controlInput == "list processing") {
              sem_wait(&mutexEntrada);
              listProc();
              sem_post(&mutexEntrada);
            } else if (controlInput == "list waiting") {
              sem_wait(&mutexEntrada);
              listWaiting();
              sem_post(&mutexEntrada);
            } else if (controlInput == "list reported") {
              sem_wait(&mutexEntrada);
              listReported();
              sem_post(&mutexEntrada);
            } else if (controlInput == "list reactive") {

            } else if (controlInput == "update") {
              cout << "\n";
              cout << "Actualizar";
              cout << "\n";
            } else if (controlInput == "all") {
              cout << "\n";
              cout << "Nivel de dendritos: " << endl;
              cout << "Nivel de piel: " << endl;
              cout << "Nivel de sangre: " << endl;
              cout << "\n";
            }
          }
        } else { cout << "Existe un error en el nombre del segmento de Memoria" << endl;}
        break;
      case '?':
        if (isprint (optopt))
          fprintf (stderr, "Opcion desconocida `-%c'.\n", optopt);
        else
          fprintf (stderr, "Opcion del caracter desconocido `\\x%x'.\n", optopt);
        return 1;
      default:
        abort ();
      }
    }
  } else if (string(argv[1]) == "rep") {
    int index;
    int c;
    opterr = 0;
    string repInput = "";
    while ((c = getopt (argc, argv, "s:im:")) != -1) {
      switch (c) {

      case's':
        if (optarg != NOMBSEGMEMORIA) {
          cout << "El argumento ingresado no es valido" << endl;
        }
        break;
      case'm':
        cout << "Obtiene examenes";
        break;
      case 'i':
        while (getline(cin, repInput)) {
          /* El condicional termina con 0 */
          if (repInput == "0") { break; }
          if (repInput.empty()) { continue; }
        }
        break;
      }
    }
  }
  close(shmfd);
  exit(0);
  return 0;
}