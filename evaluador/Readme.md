#Practica Sistemas Operativo -2015-2

####Simulación de un sistema de evaluación de exámenes médico 

**Materia**: Sistemas Operativos
**Profesor**: Juan Francisco Cardona M.

##Estudiantes

*Pablo Agudelo Marenco
*Laura María Álvarez M

##Requerimientos

* GNU make
* G++ Versión 4.7
* Compilado con c++ y -lpthread
*El makefile automáticamente compilará la fuente.

El programa evaluador recibirá en la línea de comandos la operación a realizar: 


---
####COMANDO INIT
Se encarga de dar inicio al sistema completo.Crea todo el sistema si no se encuentra ya creado.

evaluator init [-i <integer>] [-e <integer>] [-o <integer>] [-n <string>] [-b <integer>] [-d <integer>][-s <integer>] [-b <integer>]

**-i:** Número de entradas - (postivo) default=5
**-e:** Número de posiciones en cola de entrada -default= 6 posiciones
**-o:** Número de entradas en la cola de salida -default= 10
**-n:** Nombre del segmento de memoria compartida -default=evaluator
**-d:** Nivel de dentritos -default max=100
**-s:** Nivel de piel -default max:100
**-b:** Nivel de sandre -default max:100

---

####COMANDO REG
El comando reg se encarga de registrar los exÁmenes que el evaluador va
analizar. El registro puede hacerse de dos formas: interactiva o a travÉs de
varios ficheros. Si la muestra es correcta retorna un identificador.

evaluator reg [-n <string>] {{filename} ... | - }

cada registro tiene el siguiente formato:
<"integer"> {B|D|S} <"integer">
**-n:** Nombre del segmento de memoria compartida 
**filename: ** si el fichero no existe se ignora y no se env´ıa ning´un mensaje.

---
####COMANDO CTRL
Es un comando multi-propÓsito, que permite revisar cualquier parte del
sistema, configurar y reportar el estado del mismo sistema.

evaluator ctrl [-s <string>]

Subcomandos:

COMANDO  | SALIDA | PARAMETROS
--|--|--
list processing | [id i k q p]| **id:**identificador, **i:**cola de entrada, **k:**el tipo de muestra, **q:**cantidad de muestra, **p:** procesamiento en s
list waiting | [id i k q] | **id:**identificador, **i:**cola de entrada, **k:**tipo de muestra,**q:** cantidad de muestra.
list reported | [id k r] | **id:**identificador, **i:** cola de entrada, **k:** tipo de muestra, **r:** informe final de muestra.
update B/D/S <'integer'>|-|-

---

####COMANDO REP
Este comando permite obtener las muestras que han sido procesadas por el analizador y liberar el reporte de las mismas.
La siguiente es la sinopsis del comando:

evaluator rep [-s <string>] -i <integer> | -m <integer> 

**-s:** Nombre del segementro de memoria compartida
**-i:** Indica que el reporte queda en modo interativo esperando un tiempo indicado por el parametro entero en s.
**-m:** Indica que el reporte obtiene hasta un número determinado de exámenes,sino hay espera hasta completar la 

---

###ENTRADA Y SALIDA DE ARCHIVO ###

El archivo de entrada se encuentra en la carpeta /src con el nombre de *input.in* y el archivo de salida se llama *input.spl* .

---

###LIBRERIAS Y BIBLIOTECAS

* iostream
* algorithm
* sstream -> parsea cadenas y hace validaciones
* string -> atoi y metodos que usan string
* vector -> Uso dinamico de vectores
* semaphore -> Uso de semaforos y mutex
* sys/types
* signal
* cstring
* stdio
* stdlib
* ctype
* unistd
* pthread
* sys/types
* sys/mman
* fcntl.h 